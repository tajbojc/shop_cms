
from django.contrib import admin
from .models import Kategorije, Podkategorije, Kupci, Proizvodi


# Register your models here.
admin.site.register(Kategorije)
admin.site.register(Podkategorije)
admin.site.register(Kupci)
admin.site.register(Proizvodi)
