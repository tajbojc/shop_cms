from django.shortcuts import render
from django.views import generic
from .models import Kategorije, Podkategorije, Proizvodi, Kupci

# Create your views here.

class KategorijeListView(generic.ListView):
    model = Kategorije

class PodkategorijeListView(generic.ListView):
    model = Podkategorije

class ProizvodiListView(generic.ListView):
    model = Proizvodi
    print (model.objects)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['muhamedov_pro'] = "any"
        return context

class KupciListView(generic.ListView):
    model = Kupci

#-----------------------------------------------

class KategorijeAddView(generic.CreateView):
    model = Kategorije
    fields = '__all__'


class PodkategorijeAddView(generic.CreateView):
    model = Podkategorije
    #fields = '__all__'
    fields = ["opis"]

class ProizvodiAddView(generic.CreateView):
    model = Proizvodi
    fields = '__all__'

class KupciAddView(generic.CreateView):
    model = Kupci
    fields = '__all__'

