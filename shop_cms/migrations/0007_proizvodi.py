# Generated by Django 4.1.7 on 2023-03-25 15:00

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('shop_cms', '0006_rename_adress_kupci_adresa'),
    ]

    operations = [
        migrations.CreateModel(
            name='Proizvodi',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('naziv_proizvoda', models.CharField(max_length=200)),
                ('opis_proizvoda', models.TextField(max_length=1000)),
                ('Podkategorije_veza', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='shop_cms.podkategorije')),
                ('kupac_veza', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='shop_cms.kupci')),
            ],
            options={
                'verbose_name_plural': 'Proizvodi',
            },
        ),
    ]
