from django.urls import path
from . import views

urlpatterns = [
    path('', views.ProizvodiListView.as_view(), name = 'proizvodi'), 
    path('kategorije/', views.KategorijeListView.as_view(), name = 'kategorije'),
    path('podkategorije/', views.PodkategorijeListView.as_view(), name = 'podkategorije'), 
    path('proizvodi/', views.ProizvodiListView.as_view(), name = 'proizvodi'), 
    path('kupci/', views.KupciListView.as_view(), name = 'kupci'),

 #-----------------------------------------------------------------------------------
    path('kategorije-add/', views.KategorijeAddView.as_view(), name = 'kategorije-add'),
    path('podkategorije-add/', views.PodkategorijeAddView.as_view(), name = 'podkategorije-add'),
    path('proizvodi-add/', views.ProizvodiAddView.as_view(), name = 'proizvodi-add'),
    path('kupci-add/', views.KupciAddView.as_view(), name = 'kupci-add'),

]
